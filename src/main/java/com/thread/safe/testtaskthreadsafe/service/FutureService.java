package com.thread.safe.testtaskthreadsafe.service;

import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.*;
import java.util.function.Function;

@Service
public class FutureService {

    private Map<Object, Future<?>> cache = new ConcurrentHashMap<>();

    public synchronized Future<?> compute(Object key, Function function) throws ExecutionException, InterruptedException {
        if(cache.containsKey(key)){
            System.out.println("Return from cache " + cache.get(key).get());
            return cache.get(key);
        }

        Callable<Object> callable = () -> function.apply(key);
        Future future = Executors.newSingleThreadExecutor().submit(callable);
        System.out.println("Add to cache " + future.get());
        cache.put(key, future);
        return future;
    }
}
