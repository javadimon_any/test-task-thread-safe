package com.thread.safe.testtaskthreadsafe;

import com.thread.safe.testtaskthreadsafe.service.FutureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.function.Function;

@SpringBootApplication
public class TestTaskThreadSafeApplication implements CommandLineRunner {

    @Autowired
    private FutureService futureService;

    public static void main(String[] args) {
        SpringApplication.run(TestTaskThreadSafeApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Function<String, Integer> f = x -> x.length();
        futureService.compute("123", f);
        futureService.compute("456", f);
        futureService.compute("789", f);
        futureService.compute("123", f);
    }
}
